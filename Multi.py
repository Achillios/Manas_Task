from functions_multi import *

if __name__ == "__main__":
    x, y = loadData()
    m = len(y)
    # input('Press any key to continue')
    print('Normalizing Features...\n')
    x, mu, sigma = featureNormalize(x)
    X = np.c_[np.ones((m, 1)), x]
    print('Running gradient Descent')
    theta = np.zeros((3, 1))
    alpha = 0.01
    num_iters = 400
    theta, J_history = gradientMulti(X, y, theta, alpha, num_iters)
    # print('D_bug')
    # print(J_history)
    plt.plot(np.arange(1, 401), J_history, '-b', linewidth=2)
    plt.xlabel('Number of iterations')
    plt.ylabel('Cost J')
    plt.show()
    # Display gradient descent's result
    print('Theta computed from gradient descent:\n', theta)
    # Estimate the price of a 1650 sq-ft, 3 br house
    d = [[1650, 3]]
    d = np.subtract(d, mu)
    d = np.divide(d, sigma)
    d = np.c_[np.ones((1, 1)), d]
    price = np.dot(d, theta)
    print('Predicting price of a 1650 sq-ft, 3 br house:\n', price)


