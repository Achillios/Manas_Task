import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def loadData():
    data = pd.read_csv('ex2data2.txt')
    x = data.iloc[:, 0]
    x = np.c_[x, data.iloc[:, 1]]
    x = np.c_[x]
    y = data.iloc[:, 2]
    y = np.c_[y]
    data.head()
    return x, y


def plotData(x, y):
    data = pd.read_csv('ex2data2.txt')
    y = data.iloc[:, 2]
    pos = y[:] == 1
    neg = y[:] == 0
    plt.plot([x[pos][:, 0]], [x[pos][:, 1]], color='black', marker='+', markersize=7, linewidth=2)
    plt.plot([x[neg][:, 0]], [x[neg][:, 1]], color='yellow', marker='o', markersize=7)
    plt.xlabel('Microchip Test 1')
    plt.ylabel('Microchip Test 2')


def sigmoid(z):
    g = np.zeros((np.size(z, 0), np.size(z, 1)))
    temp = 1 + np.exp(-z)
    g = np.divide(1, temp)
    return g


def mapFeature(x1, x2):
    x1 = x1.T
    x2 = x2.T
    degree = np.arange(1, 7)
    out = np.ones((np.size(x1, 0), 1))
    for i in degree:
        loop = np.arange(0, i + 1)
        for j in loop:
            s1 = np.power(x1, (i - j))
            s2 = np.power(x2, j)
            ans = np.multiply(s1, s2)
            out = np.c_[out, ans]
    return out


def costFunctionReg(theta, x, y, lamda):
    m = len(y)
    J = 0
    grad = np.zeros((np.size(theta, 0), 1))
    z = np.dot(x, theta)
    h = sigmoid(z)
    temp = theta
    # making theta0 as 0
    temp[0, 0] = 0
    p = (lamda / 2 * m) * np.dot(temp.T, temp)
    o_y = np.subtract(1, y).T
    s1 = np.dot((-y.T), (np.log(h)))
    s2 = np.dot(o_y, np.log(np.subtract(1, h)))
    s = np.subtract(s1, s2)
    J = (1 / m) * np.sum(s) + p
    grad = np.dot(x.T, np.subtract(h, y)) + np.dot(lamda, temp)
    grad = grad / m
    return J, grad
