import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def loadData():
    data = pd.read_csv('ex1data2.txt')
    x = data.iloc[:, 0]
    x = np.c_[data.iloc[:, 1], x]
    y = data.iloc[:, 2]
    y = np.c_[y]
    m = len(y)
    data.head()
    return x, y


def featureNormalize(x):
    m = np.size(x, 0)
    n = np.size(x, 1)
    mu = np.zeros((1, n))
    sigma = np.zeros((1, n))
    mu[0, 0] = np.mean(x[:, 0])
    mu[0, 1] = np.mean(x[:, 1])
    sigma[0, 0] = np.std(x[:, 0])
    sigma[0, 1] = np.std(x[:, 1])
    # print(mu)
    # print(sigma)
    xm = np.subtract(x, mu)
    xn = np.divide(xm, sigma)
    return xn, mu, sigma


def gradientMulti(x, y, theta, alpha, num_iters):
    m = y.size
    J_history = np.zeros(num_iters)
    ite = 0
    while ite != num_iters:
        h = np.dot(x, theta)
        theta = theta - alpha * (1 / m) * (x.T.dot(h - y))
        J_history[ite] = computeCostMulti(x, y, theta)
        ite = ite + 1
    return theta, J_history


def computeCostMulti(x, y, theta):
    h = np.dot(x, theta)
    m = len(y)
    temp = np.subtract(h, y)
    temp = np.power(temp, 2)
    temp = np.sum(temp)
    cost = temp / (2 * m)
    return cost
