from classification_reg_functions import *

# Load Data
# The first two columns contains the X values and the third column
# contains the label (y).

x, y = loadData()
# plot data
plotData(x, y)
plt.show()

# =========== Part 1: Regularized Logistic Regression ============

# Add Polynomial Features
X = mapFeature(x[:, 0], x[:, 1])

# Initialize fitting parameters
initial_theta = np.zeros((np.size(X, 1), 1))

# Set regularization parameter lambda to 1
lamda = 1

# Compute and display initial cost and gradient for regularized logistic regression
cost, grad = costFunctionReg(initial_theta, X, y, lamda)

print('Cost at initial theta (zeros): \n', cost)
print('Expected cost (approx): 0.693\n')
print('Gradient at initial theta (zeros) - first five values only:\n')
print('\n', grad[0:6])
print('Expected gradients (approx) - first five values only:\n')
print(' 0.0085\n 0.0188\n 0.0001\n 0.0503\n 0.0115\n')

input('\nProgram paused. Press enter to continue.\n')

# Compute and display cost and gradient with all-ones theta and lamda = 10
test_theta = np.ones((np.size(X, 1), 1))
cost, grad = costFunctionReg(test_theta, X, y, 10)

print('\nCost at test theta (with lambda = 10): \n', cost)
print('Expected cost (approx): 3.16\n')
print('Gradient at test theta - first five values only:\n')
print('\n', grad[0:6])
print('Expected gradients (approx) - first five values only:\n')
print(' 0.3460\n 0.1614\n 0.1948\n 0.2269\n 0.0922\n')

input('\nProgram paused. Press enter to continue.\n')
